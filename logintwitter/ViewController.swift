//
//  ViewController.swift
//  logintwitter
//
//  Created by user on 01/02/18.
//  Copyright © 2018 Contus. All rights reserved.
//

import UIKit
import TwitterKit

class ViewController: UIViewController {

    
    @IBOutlet weak var loginImage: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var name: UILabel!
    
    var loginButton : TWTRLogInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        loginButton = TWTRLogInButton{(session, error) in
            
            if let unwrappedsession = session{
                let client = TWTRAPIClient()
                client.loadUser(withID: (unwrappedsession.userID), completion: { (user, error) in
                    self.username.text = user?.name
                    self.name.text = unwrappedsession.userName
                     let imgurl = user?.profileImageURL
                    let url = URL(string: imgurl!)
                    let data = try?Data (contentsOf: url!)
                    self.loginImage.image = UIImage (data: data!)
                })
            }
            else
            {
                print("login error")
            }
    }
        loginButton.center = self.view.center
//        loginButton.center = CGRect(x: 30, y: 30, width: 150, height: 150)
        self.view.addSubview(loginButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

